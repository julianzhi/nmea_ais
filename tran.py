import ais
import ais.nmea_messages
import numpy as np
import queue 
import serial
import pynmea2 as nm
import sys
import time
import json
import socket
from threading import Thread
import logging
logger = logging.getLogger('tran.py') 


class read_NMEA0183():
    def __init__(self, nmea_queue,serial_port,use_checksum):
        '''
        Initiates variables.

        Keyword arguments:
        location -- the location of the serial connection
        baud_rate -- the baud rate of the connection
        timeout -- the timeout of the connection

        '''
        self.exit = False
        self.nmea_queue = nmea_queue
        self.serial_port = serial_port
        self.use_checksum = use_checksum
        self.serial_dev = None
        self.serial_data = None


    def start(self):
        '''
        Creates a thread to read serial connection data.
        '''
        self.serial_dev = self.serial_port
        serial_thread = Thread(None,self.read_thread,None,())
        serial_thread.start()
 
    def read_thread(self):
        '''
        The thread used to read incoming serial data.
        '''
        dat_new = b''
        dat_old = b''
        #Loops until the connection is broken, or is instructed to quit
        #try:
        while self.is_open():
            #Instructed to quit
            if self.exit: 
                break
            if dat_new: 
                dat_old = dat_new
                dat_new = b''
            dat_old = dat_old + self.buffer()
            if dat_old.find(b'\r\n') != -1:
                try:
                    self.serial_data, dat_new = dat_old.split(b'\r\n')
                except:
                    print('except',dat_old)
                    pass
                #The checksum is correct, so the data will be deconstructed
                #if self.checksum(self.serial_data) or not self.use_checksum:
                if self.serial_data is not None:
                    self.nmea_queue.put( (time.time() ,b''.join([self.serial_data,b'\r\n']))  )
                dat_old = b''
        #except:
        #    self.quit()

    def is_open(self):
        '''
        Checks whether the serial connection is still open.
        '''
        return self.serial_dev.isOpen()

    def buffer(self):
        '''
        Creates a buffer for serial data reading. Avoids reading lines for better performance.
        '''
        dat_cur = self.serial_dev.read(1)
        x = self.serial_dev.inWaiting()
        if x: dat_cur = dat_cur + self.serial_dev.read(x)
        return dat_cur

    def quit(self):
        '''
        Enables quiting the serial connection.
        '''
        self.exit = True


def get_dist(sx,sy,x,y):
    sx = sx
    sy = sy
    x = x
    y = y
    
    dx = (sx-x)/180.0*np.pi       #long
    dy = (sy-y)/180.0*np.pi       #lat
    lat = y/180.0*np.pi
    lat2 = y/180.0*np.pi
    a = np.sin(dy*0.5)**2+np.cos(lat)*np.cos(lat2)*np.sin(dx*0.5)**2
    return 6373.0*2*np.arctan2(np.sqrt(a),np.sqrt(1-a))

            
            

 
s_ais = serial.Serial(port="/dev/ttyUSB0",baudrate=4800,timeout=20,write_timeout=0)
s_boot = serial.Serial(port="/dev/ttyUSB1",baudrate=4800,timeout=20,write_timeout=0)
cs = socket.socket(socket.AF_INET,socket.SOCK_DGRAM)
cs.setsockopt(socket.SOL_SOCKET,socket.SO_REUSEADDR,1)
cs.setsockopt(socket.SOL_SOCKET,socket.SO_BROADCAST,1)    

nmealog_ais = open('nmealog_ais_'+time.strftime('%Y-%m-%d_%H-%M'),'w')
nmealog_boot = open('nmealog_boot_'+time.strftime('%Y-%m-%d_%H-%M'),'w')

msg_types = json.load(open('bootmsgtypes'))
msgs = [{i:0} for i in msg_types]

ships = dict()


outqueue = queue.PriorityQueue()
inqueue = ais.vdm.BareQueue()

q_nmea_boot = queue.Queue()
nmea_boot = read_NMEA0183(nmea_queue=q_nmea_boot,serial_port=s_boot,use_checksum=False)
q_nmea_ais = queue.Queue()
nmea_ais = read_NMEA0183(nmea_queue=q_nmea_ais,serial_port=s_ais,use_checksum=False)

nmea_ais.start()
nmea_boot.start()
x = 0.0
y = 0.0


while 1:
    while q_nmea_boot.qsize():
        msgtime,line = q_nmea_boot.get_nowait()
        q_nmea_boot.task_done()
        cs.sendto(line,('255.255.255.255',2947))
        line_asc = line.decode('ascii')
        nmealog_boot.write(str(msgtime)+";"+line_asc)
        try:
            msg = nm.NMEASentence.parse(line_asc)
        except:
            logger.error('NMEA MSG error' + line_asc)
            continue
        
        if (msgtime-msgs[msg.sentence_type])>1.0:
            s_boot.write(line)
            msgs[msg.sentence_type] = msgtime 
        
        if msg.sentence_type == 'GLL':
            x = msg.longitude
            y = msg.latitude
                
    while q_nmea_ais.qsize():
        tstamp,line = q_nmea_ais.get_nowait()
        q_nmea_ais.task_done()
        cs.sendto(line,('255.255.255.255',2947))
        line_asc = line.decode('ascii')
        nmealog_ais.write(str(tstamp)+";"+line_asc)
        inqueue.put(line_asc)

    DELTA = 10.0
    if x != 0.0 and y != 0.0:
        while inqueue.qsize():
            el = inqueue.get_nowait()
            try:
                msgid =  el['decoded']['id']
                mmsi = el['decoded']['mmsi']
                if mmsi not in ships:
                    ships.update({mmsi:{'MSGS' : dict(),'INFO' : dict()}})
                    ship = ships[mmsi]
                    ship['MSGS'].update({msgid :  [el['decoded'],el['lines']] })
                    ship['INFO'].update(el['decoded'])
                else:
                    ship = ships[mmsi]
                    ship['MSGS'].update({msgid :  [el['decoded'],el['lines']] })
                    ship['INFO'].update(el['decoded'])
            except:
                logger.error('AIS MSG error' + el)
                inqueue.task_done()
                continue
            inqueue.task_done()
            try: 
                sx = ship['INFO']['x']
                sy = ship['INFO']['y']
            except:
                continue
            dist = get_dist(sx,sy,x,y)
            if dist<DELTA:
                outqueue.put([ dist,el['lines'],dist])
    
    
    #time.sleep(0.05)
    
    if s_boot.out_waiting < 82*3:
        try:
            el = outqueue.get_nowait()
            for i in el[1]:
                tmp = bytearray(i,'ascii')
                tmp.append(13)
                tmp.append(10)
                s_boot.write(tmp)
            outqueue.task_done()
        except queue.Empty:
            break
    
    if len(ships.keys()) > 1000:
        ships = dict()   
    if outqueue.qsize() > 20:
        for i in range(outqueue.qsize()):
            el = outqueue.get_nowait()
            outqueue.task_done()
    sys.stdout.write('\r'+str(x)+"  "+str(y)+"  "+str(len(ships.keys()))+"   "+str(inqueue.unfinished_tasks)+"  "+str(inqueue.qsize())+"   "+str(outqueue.unfinished_tasks)+"  "+str(outqueue.qsize())+"   "+str(q_nmea_ais.qsize())+"   "+str(q_nmea_ais.unfinished_tasks)+"   "+str(q_nmea_boot.qsize())+"   "+str(q_nmea_boot.unfinished_tasks) )

    
    
    